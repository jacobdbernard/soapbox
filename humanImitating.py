import soapbox
import random
import re
from time import sleep
import tweepy
import os
import requests
import proPear
import DBInteraction

#from yulduzcredentials import *

# function for checking if tweets are ok to be reposted/liked/retweeted, ok being non-political and not having any bad words in them
# the tweet is to be passed as a string
# allowRetweets and allowMedia do exactly what would be expected and are allowed unless otherwise specified
# allowDeepReplys doesn't allow tweets that are replys


def tweetIsOk(tweet, allowRetweets=True, allowLinks=True, allowReplys=True, allowMedia=True):
        # simultaneously removing any characters that are not number or letters as well as changing all characters to lowercase so that they match the list
    cleanedTweet = re.sub('[^a-z0-9@ ]+', '', tweet.text.lower())
    bannedWordsFile = open('politicalWords.txt', 'r')
    bannedWords = bannedWordsFile.read().splitlines()
    bannedWordsFile.close()

    if not allowRetweets:
        bannedWords.append('rt')
        bannedWords.append('retweet')

    if not allowLinks:  # blocks media by added 'tco' to the banned words list, https://t.co/...... is how media uploaded in tweets appears in tweet text form
        bannedWords.append('tco')
        bannedWords.append('https')

    if not allowMedia:
        if 'media' in tweet.entities:
            return False

    if not allowReplys:
        if tweet.in_reply_to_status_id is not None:
            # print('tweet is a reply, skipping')
            return False

    for word in bannedWords:
        if word in cleanedTweet:
            # print('Tweet contained banned word, skipping')
            return False
    # print(cleanedTweet)
    return True

    # this function will follow users whose tweets appear in a given query
    # api = API object of the account to be acted on, query = term to be searched, interval = lower bound of seconds between follows


def followFromSearch(api, interval, query):
    for tweet in tweepy.Cursor(api.search, q=query).items(10):
        try:
            if not tweet.user.following and tweet.user.lang == 'en':
                # print(tweet.user.screen_name + '\n')
                tweet.user.follow()
                num = random.randrange(7) + interval
                sleep(num)
        except tweepy.TweepError as e:
            print(e.reason)
    sleep(interval * 4)

    # ExpandFromFollowers a function that expands the number of accounts followed by following friends(followers) of followers
    # api = api of account to be acted on, interval is the interval between follows, numFollowers is the number of followers to expand from,
    # toFollowPer is the number of users to follow per each follower to be expanded from

def okToFollow(api):
    numFollowers = len(api.followers_ids(api.me().id))
    numFollowing = len(api.friends_ids())

    if numFollowing - numFollowers > 20:
        print(api.me().screen_name + ' has a following differential too high')
        return False
    else:
        return True

def expandFromFollowers(api, interval, numFollowers, toFollowPer):
    followers = api.followers(api.me().id)
    following = api.friends_ids()
    # appending users id to following so that an account doesn't try and follow itself
    following.append(api.me().id)

    if numFollowers > len(followers):
        print(api.me().screen_name + 'doesnt have enough followers to expand from, exiting function')
        return

    for x in range(0, numFollowers):
        followersOfFollower = api.followers(followers[x].id)
        followersOfFollower.reverse()  # reversing fof as to get the oldest friends first
        for y in range(0, toFollowPer):
            # following back if not already following that person
            if followersOfFollower[y].id not in following and followersOfFollower[y].lang == 'en':
                api.create_friendship(followersOfFollower[y].id)
            else:
                toFollowPer += 1  # increasing by 1 if not able to follow selecte user
            # randrange interval should be increased later when implemented
            num = random.randrange(3) + interval
            sleep(num)
        # randrange interval should be increased later when implemented
        num = random.randrange(6) + interval
        sleep(num)

    sleep(interval * 4)

    # This function aims to repost tweets that appear from a given search as the accounts own, if they are not from a follower
    # api = API object of the account to be acted on, query = term to be searched, interval = lower bound of seconds between follows,
    # numToReport = number of tweet to be reposted, withMedia is a bool that specifies if tweets that contain media should be allowed or not


def repostTweetFromSearch(api, interval, query, numToRepost, withMedia=False, withURL=False):
    followerAndFollowing = api.followers(api.me().id) + api.friends_ids()
    tweetsPosted = 0

    for tweet in tweepy.Cursor(api.search, q=query, lang='en').items(100):
        if tweetIsOk(tweet, False, withURL, False, withMedia) and tweet.user.id not in followerAndFollowing: #checking if the tweet is ok and that the tweet isn't by a user that is following us or being followed
            if not withMedia:
                print(api.me().screen_name + ' reposting tweet without media')
                # should maybe put this in an exception handler
                api.update_status(tweet.text)
                tweetsPosted += 1
                num = random.randrange(30) + interval
                sleep(num)
            elif withMedia:
                if 'media' in tweet.entities and tweet.entities['media'][0]['type'] == 'photo':
                    print(api.me().screen_name + ' reposting tweet with media')
                    for url in tweet.entities['media']:
                        # print(url['media_url'])
                        tweetImageFromURL(api, url['media_url'], tweet.text[0:len(tweet.text)-24])
                        tweetsPosted += 1

        if tweetsPosted >= numToRepost:
            sleep(interval * 3)
            return True


def tweetImageFromURL(api, url, tweet):
    filename = 'tempPic.jpg'
    request = requests.get(url, stream=True)
    if request.status_code == 200:
        with open(filename, 'wb') as image:
            for chunk in request:
                image.write(chunk)
        api.update_with_media(filename, status=tweet)
        os.remove(filename)
    else:
        print("Unable to download image")

        # followBack aims to follow back users that have recent followed a bot if the bot doesn't already follow them
        # api = API object of the account to be acted on, interval = lower bound of seconds between follows, num = number of accouts to follow back


def followBack(api, interval, numToFollow):
    followers = api.followers(api.me().id)
    numFollowers = len(followers)
    following = api.friends_ids()

    counter = 0  # counter to keep track of iterations through the loop so that we only iterate through as many times as the account follows
    followed = 0  # counter to keep track of how many accounts have been followed-back
    following = api.friends_ids()

    print(api.me().screen_name + ' is following back ' +
          str(numToFollow) + ' users')

    while counter < numFollowers:
        # following back if not already following that person
        if followers[counter].id not in following and followers[counter].lang == 'en':
            # print('attempting to follow')
            api.create_friendship(followers[counter].id)
            counter += 1
            followed += 1
            if counter > numFollowers or followed >= numToFollow:
                break
        else:
            counter += 1
            # print(counter)
        # randrange interval should be increased later when implemented
        num = random.randrange(3) + interval
        sleep(num)

    # increasing the end sleep interval to allow more of a gap between activities, * 3 is subject to change
    sleep(interval*3)

    # function to unfollow accounts that haven't followed an account back starting at the oldest
    # this function will run until the required number of accounts have been unfollowed, or the entire list of accounts being followed has been traversed
    # api = API object of the account to be acted on, interval = lower bound of seconds between follows, numToUnfollow = number of accounts to be unfollwed


def unfollowNotFollowing(api, interval, numToUnfollow):
    following = api.friends_ids()
    following.reverse()  # reversing the list of users being followed as to start with the ones who have been followed the longest
    followers = api.followers_ids()
    counter = 0
    unfollowed = 0
    print(api.me().screen_name + ' is unfollowing dirty non-followbackers')
    while counter < len(following):
        if following[counter] not in followers:
            api.destroy_friendship(following[counter])
            # print('unfollowwwinng')
            unfollowed += 1
            if unfollowed >= numToUnfollow:
                sleep(interval*4)
                break
        counter += 1
        # randrange interval should be increased later when implemented
        num = random.randrange(3) + interval
        sleep(num)

    # function to simply randomly unfollow a set number of accounts already followed
    # this function will run until the required number of accounts have been unfollowed, or the entire list of accounts being followed has been traversed
    # api = API object of the account to be acted on, interval = lower bound of seconds between follows, numToUnfollow = number of accounts to be unfollwed


def unfollowRandom(api, interval, numToUnfollow):
    following = api.friends_ids()
    unfollowed = 0

    while unfollowed < numToUnfollow:
        # checking to make sure that all followers aren't unfollwed
        if numToUnfollow >= len(following):
            break
        else:
            # print('unfollowing')
            # picking a random follower from the list of users that the acount follows
            api.destroy_friendship(following[random.randrange(len(following))])
            unfollowed += 1
        # randrange interval should be increased later when implemented
        num = random.randrange(3) + interval
        sleep(num)
    print(api.me().screen_name + 'is pausing')
    sleep(interval * 4)

    # function to randomly perform  a specified number of retweets/likes on an accoutns timeline to simulate normal twitter usage
    # api = API object of the account to be acted on, interval = lower bound of seconds between follows, numActions = number of tweets to be acted on
    # tweets will be liked, retweeted, or both


def timelineActions(api, interval, numActions):
    if numActions > 20:
        print("As only 20 tweets are able to be pulled from the timeline at once more than 20 actions cannot be performed")
        sleep(interval * 3)
        return


    timeline = (api.home_timeline())  # pulling the timeline
    counter = 0
    tweets = []  # list of the tweets that will be acted on (0-19)

    while counter < numActions:  # generating a list of 20 random ints 0-19 that will be the tweets to be acted on
        tempnum = random.randrange(20)
        if tempnum not in tweets:
            tweets.append(tempnum)
            counter += 1

    if numActions > len(timeline):
        print('More actions than tweets in the timeline, exiting function')
        return

    tweets.sort()  # sort the tweets so that the user appears to start at the top of the feed and move down

    allowRetweets = True

    while numActions > 0 :
        # generating a random number that will help decide to like, retweet, or do both to a given tweet on the timeline
        tempnum = random.randrange(10)
        # 6 (which could produce 0-5) is used to give favorites a higher chance, which are more commonly given out than retweets

        # 40% to not allow retweets as retweeting a high amount of retweets can be undesirable
        if random.randrange(9) > 5:
            allowRetweets = False

        # ensuring a tweet is 'ok' that is it does not contain a bad or political term
        if(tweetIsOk(timeline[0], allowRetweets)):
            if tempnum == 0:  # if 0 do both, most unlikely
                print(api.me().screen_name + ' is doing both!')
                try:
                    api.create_favorite(timeline[0].id)
                    api.retweet(timeline[0].id)
                except tweepy.TweepError:  # catching exceptions just in case the tweet has already been acted on
                    print(
                        'Error! Tweet has already been favorited or tweeted, skipping')
            elif tempnum < 3:  # if even number retweet tweet
                print(api.me().screen_name + ' just retweeted a tweet')
                try:
                    api.retweet(timeline[0].id)
                except tweepy.TweepError:
                    print(
                        'Error! Tweet has already been favorited or tweeted, skipping')
            else:  # otherwise the tweet will be odd and will be favorited
                print(api.me().screen_name + ' just favorited a tweet')
                try:
                    api.create_favorite(timeline[0].id)
                except tweepy.TweepError:
                    print(
                        'Error! Tweet has already been favorited or tweeted, skipping')

        numActions -= 1
        timeline.pop(0)  # remove that number from the timeline
        num = random.randrange(3) + interval
        sleep(num)

    sleep(interval * 3)


if __name__ == '__main__':
    """auth = tweepy.OAuthHandler(consumerKey, consumerSecret)
    auth.set_access_token(accessToken, accessSecret)
    api = tweepy.API(auth)
    """
    package = DBInteraction.authenticateUser(1)
    api = package[0]

    # print('well hello there')

    # followFromSearch(api, 20, 'soccer')
    #followBack(api, 100, 4)
    # unfollowNotFollowing(api,7,1)
    # unfollowRandom(api,6,1)
    #timelineActions(api,5,3)
    #repostTweetFromSearch(api,9,'soccer',1,True,True)
    # expandFromFollowers(api,5,2,2)
    #print(okToFollow(api))
    proPear.promotePears(api, 5)