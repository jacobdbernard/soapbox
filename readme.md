This is a bot for automating an account on twitter to act like a normal user and grow in number of followers while not breaking any twitter rules. It does this through use of the [Tweepy Python Twitter API.](https://tweepy.readthedocs.io/en/v3.6.0/)

The project was created with the idea to run a large number of Twitter accounts with it and have those gain popularity, and then they could be used in order to try and influence public opinion in a measurable way. In order to be an ethical researcher and have quantifiable results the topic chosen was Pears Vs. Apples. Twitter has recently make it more significantly difficult to get API keys for an account and thus run it through the API so this never happened, but it successfully ran on two test accounts. 

humanImitating.py contains all of the functions that all imitate human usages of Twitter, and soapbox.py is the "scheduler" that will take an accounts API keys from the database (the database interaction functions are in DBInteraction.py) and use functions from humanImitating.py in order to do so. 

