import tweepy
import random
import humanImitating
import time
import proPear
import DBInteraction

import pymysql.cursors
from time import sleep

def newAccountScheduler(api):

    canFollow = False
    unfollowBonus = 2

    if humanImitating.okToFollow(api): #checking if the following/follower differential is acceptable
        canFollow = True
        unfollowBonus = 0
    
    try:
        if random.randrange(10) < 8 and canFollow:  # 80% chance to followback those who have followed
            print('Following back')
            humanImitating.followBack(api, 12, 4)

        if random.randrange(10) < (1 + unfollowBonus):  # 10% - 30% chance of unfollowing random followers
            print('Unfollowing random')
            humanImitating.unfollowRandom(api, 15, 1 + unfollowBonus)

        if random.randrange(10) < (3 + unfollowBonus):  # 30% - 50% chance to unfollow those not following back
            print('Unfollowing those not following')
            humanImitating.unfollowNotFollowing(api, 15, 1 + unfollowBonus)

        if random.randrange(10) > 6:  # 30% chance to do random timeline actions
            print('Doing timeline actions')
            humanImitating.timelineActions(api, 11, 4 - unfollowBonus)

        querys = ['kitten','follow back']
        if random.randrange(10) < 5 and canFollow:  # 50% chance to follow random accounts from a query
            print('Following random from query')
            humanImitating.followFromSearch(api, 15, querys[random.randrange(1)])

        searchQuerys = ['cat', 'dog', 'whale', 'kitten', 'apple',
                  'follow back', 'music', 'pear', 'sleep', 'ice cream']
        if random.randrange(10) > 6:  # 30% to repost content as it's own
            withImage = False
            # 40% chance to repost a tweet that contains an image
            if random.randrange(5) > 2:
                #print('Reposting content with an image')
                withImage = True
            #else:
                #print('Reposting content without an image')
            
            humanImitating.repostTweetFromSearch(api, 60, searchQuerys[random.randrange(10)], 1, withImage)

        if random.randrange(10) > 4 and canFollow:  # 50% chance to follow friends of friends
            print('Following friends of friends')
            humanImitating.expandFromFollowers(api, 8, random.randrange(2)+1, random.randrange(2)+1)

    except tweepy.TweepError as err:
        print("Tweepy error has occured: " + err.reason)
        if(err.reason == 'RateLimitError'):
            print('Rate limit error')
            sleep(60*12)
        else:
            return


def normalActionsScheduler(api):

    canFollow = False
    unfollowBonus = 2

    if humanImitating.okToFollow(api): #checking if the following/follower differential is acceptable
        canFollow = True
        unfollowBonus = 0

    try:
        if random.randrange(10) < 5 and canFollow:  # 50% chance to followback those who have followed
            print('Following back')
            humanImitating.followBack(api, 12, 3)

        if random.randrange(10) < (1 + unfollowBonus):  # 10% - 30% chance of unfollowing random followers
            print('Unfollowing random')
            humanImitating.unfollowRandom(api, 15, 4)

        if random.randrange(10) < (4 + unfollowBonus):  # 40% - 60% chance to unfollow those not following back
            print('Unfollowing those not following')
            humanImitating.unfollowNotFollowing(api, 7, 5 + unfollowBonus)

        if random.randrange(10) > 2:  # 70% chance to do random timeline actions
            print('Doing timeline actions')
            humanImitating.timelineActions(api, 11, 6)

        querys = ['cat', 'dog', 'whale', 'kitten', 'apple',
                  'follow back', 'music', 'pear', 'sleep', 'ice cream']
        if random.randrange(10) < 2 and canFollow:  # 20% chance to follow random accounts from a query
            print('Following random from query')
            humanImitating.followFromSearch(api, 20, querys[random.randrange(10)])

        if random.randrange(10) > 4:  # 50% to repost content as it's own
            withImage = False
            # 40% chance to repost a tweet that contains an image
            if random.randrange(5) > 2:
                #print('Reposting content with an image')
                withImage = True    
            #else:
                #print('Reposting content without an image')
            humanImitating.repostTweetFromSearch(api, 60, querys[random.randrange(10)], 1, withImage)

        if random.randrange(10) > 6 and canFollow: 
            print('Following friends of friends')
            humanImitating.expandFromFollowers(api, 8, random.randrange(3)+1, random.randrange(2)+1)

    except tweepy.TweepError as err:
        print("Tweepy error has occured: " + err.reason)
        if(err.reason == 'RateLimitError'):
            print('Rate limit error')

def metaScheduler(accountId):

    package = DBInteraction.authenticateUser(accountId)
    api = package[0]
    cycles = package[1]
    # for now just normal actions
    
    if time.localtime(time.time()).tm_hour == 1:
        print('sleeping')
        sleep(3600 * 8)  # sleep at night
        
    if cycles > 20:
        normalActionsScheduler(api)
    else:
        print(api.me().screen_name + ' has less than 20 cycles completed, using new account scheduler')
        newAccountScheduler(api)
        
    DBInteraction.incrementCounter(accountId) #incrementing the counter that keeps track of number of cycles run

    sleepTime = (random.randrange(5)+5)*60
    print('Taking a break for ' + str(sleepTime/60) + ' minutes')
    sleep(sleepTime)

    if(accountId == 1):
        metaScheduler(2)
    else:
        metaScheduler(1)


if __name__ == '__main__':
    metaScheduler(2)
